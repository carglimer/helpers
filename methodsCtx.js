const interval = 24 * 60 * 60 * 1000
let ctx // ctx = $g da ara this
export function initContext(_ctx) {
  ctx = _ctx
}

export const getSystemDateText = (locale = 'de', dateMillis) => {
  const now = Date.now()
  const startOfDay = Math.floor(dateMillis / interval) * interval
  if (now - startOfDay < interval) {
    // heute
    return ctx.t('qchat.chat.time.today') // 'heute' // this.t('qchat.questionnaire.upload.error.text')
  } else if (now - startOfDay > interval && now - startOfDay < 2 * interval) {
    // gestern
    return ctx.t('qchat.chat.time.yesterday')
  } else {
    const date = new Date(dateMillis) // date.getDate() aris dge   14 JUNI 2019
    const options = { year: 'numeric', month: 'long', day: 'numeric' }
    const resp = date.toLocaleDateString(locale, options) // locale = 'de', en, ar
    // console.log('date= ', resp)
    return resp
  }
}
export const getExtensionFromFilename = (filename) => {
  if (!filename) {
    return ''
  }
  const parts = filename.split('.')
  return parts.length > 1 ? parts.pop() : ''
}
export const getFilenamefromPath = (path) => {
  if (path) {
    return path.substring(path.lastIndexOf('/') + 1)
  }
  return ''
}

// თარგმანისთვის არის საჭირო
/* export function jsonWorker(jsonKey) {
  const t = ctx.t(jsonKey) // auto_ankauf
  const obj = {}
  for (const key in t) {
    obj[key] = t[key] // werbeText: this.$t('auto_ankauf.werbeText'),
  }
  return obj
} */

export function isMobileBrowser() {
  const windowWidth = window.screen.width < window.outerWidth ? window.screen.width : window.outerWidth
  let isMobile = false
  if (
    navigator.userAgent.match(/android/i) ||
    navigator.userAgent.match(/webos/i) ||
    navigator.userAgent.match(/iphone/i) ||
    navigator.userAgent.match(/ipad/i) ||
    navigator.userAgent.match(/ipod/i) ||
    navigator.userAgent.match(/blackberry/i) ||
    navigator.userAgent.match(/windows phone/i)
  ) {
    isMobile = true
  }
  if (windowWidth < 501 || isMobile) {
    // alert("You're using Mobile Device!!")
    return true
  } else {
    return false
  }
  // alert("You're using Desktop Device!!")
}
export const mapSync = (data) => {
  const result = {}
  for (const [key, prop] of Object.entries(data)) {
    result[key] = {
      get() {
        return this[prop]
      },
      set(val) {
        this.$emit('update:' + prop, val)
      }
    }
  }
  return result
}
export function getFileDataText(idbMedia) {
  if (idbMedia && idbMedia.media) {
    let filename
    let fileSize
    if (idbMedia.media.blob) {
      fileSize = idbMedia.media.blob.size
      filename = idbMedia.media.blob.name
    } else if (idbMedia.media.relPath) {
      fileSize = idbMedia.media.size
      filename = getFilenamefromPath(idbMedia.media.relPath)
    }
    if (fileSize) {
      let sizeText = ''
      if (fileSize > 0 && fileSize < 1000) {
        sizeText = fileSize + ' bt'
      } else if (fileSize >= 1000 && fileSize < 1000000) {
        sizeText = Math.ceil(fileSize / 1000) + ' kb'
      } else if (fileSize >= 1000000) {
        sizeText = Math.ceil(fileSize / 1000000) + ' mb'
      }
      // let filename = file.name
      const maxlength = ctx.gdata.filenameMaxlength
      if (filename.length > maxlength) {
        const ext = getExtensionFromFilename(filename)
        filename = filename.substr(0, maxlength) + '.. .' + ext
      }
      const text = filename + '  (' + sizeText + ')'
      ctx.clog('fsl text ', text)
      return text
    }
  }
  return null
}
export function getFilenameText(idbMedia) {
  if (idbMedia && idbMedia.media) {
    let filename
    if (idbMedia.media.blob) {
      filename = idbMedia.media.blob.name
    } else if (idbMedia.media.relPath) {
      filename = getFilenamefromPath(idbMedia.media.relPath)
    }
    if (filename) {
      const maxlength = ctx.gdata.filenameMaxlength
      if (filename.length > maxlength) {
        const ext = getExtensionFromFilename(filename)
        filename = filename.substr(0, maxlength) + '.. .' + ext
      }
      return filename
    }
  }
  return null
}
