export const events = {
  oppositeLocale: 'oppositeLocale',
  acknowlidgeServerAboutGotAllUserMessages: 'acknowlidgeServerAboutGotAllUserMessages',
  acknowlidgeServerAboutReadAllUserMessages: 'acknowlidgeServerAboutReadAllUserMessages',
  checkYesnoConnections: 'checkYesnoConnections',
  mediaMaxSizeExeeded: 'mediaMaxSizeExeeded',
  expandYnc: 'expandYnc',
  valide: 'valide',
  goToRoom: 'goToRoom',
  goToChat: 'goToChat',
  goToQuestionnaire: 'goToQuestionnaire',
  setPendingMessage: 'setPendingMessage',
  handleReceivedPendingMessage: 'handleReceivedPendingMessage',
  sendPendingMessages: 'sendPendingMessages',
  socketConnectError: 'socketConnectError',
  invoiceDownload: 'invoiceDownload',
  invoiceDownloaded: 'invoiceDownloaded',
  invoiceGetSucceeded: 'invoiceGetSucceeded',
  invoiceDownloadError: 'invoiceDownloadError',
  invoiceUpload: 'invoiceUpload',
  invoiceUploaded: 'invoiceUploaded',
  invoiceUploadError: 'invoiceUploadError',
  invoiceUploadedForUploader: 'invoiceUploadedForUploader',
  invoiceUploadErrorForUploader: 'invoiceUploadErrorForUploader',
  chatMediaStartLoading: 'chatMediaStartLoading',
  chatMediaStopLoading: 'chatMediaStopLoading',
  mediaDialogOpen: 'mediaDialogOpen',
  questionnaireMediaStartLoading: 'questionnaireMediaStartLoading',
  questionnaireMediaUploadProgress: 'questionnaireMediaUploadProgress',
  questionnaireUploadTsArr: 'questionnaireUploadTsArr',
  questionnaireUpload: 'questionnaireUpload',
  questionnaireBlurred: 'questionnaireBlurred',
  questionnaireCheckerValidation: 'questionnaireCheckerValidation',
  questionnaireCheckerValidationChanged: 'questionnaireCheckerValidationChanged',
  questionnaireUploaded: 'questionnaireUploaded',
  questionnaireUploadError: 'questionnaireUploadError',
  questionnaireUploadedForUploader: 'questionnaireUploadedForUploader',
  questionnaireUploadErrorForUploader: 'questionnaireUploadErrorForUploader',
  questionItemOpen: 'questionItemOpen',
  hideQuestionItem: 'hideQuestionItem',
  mediaUploadError: 'mediaUploadError',
  chatMediaUploadProgress: 'chatMediaUploadProgress',
  fileUploadProgress: 'fileUploadProgress',
  fileDownloadProgress: 'fileDownloadProgress',
  initChat: 'initChat',
  loadQuestionnaire: 'loadQuestionnaire',
  loadReceiverAllMessages: 'loadReceiverAllMessages',
  idbMediasChatCW: 'idbMediasChatCW',
  idbMediasChat: 'idbMediasChat',
  idbMediasQuestionnaire: 'idbMediasQuestionnaire',
  connection: 'connection',
  disconnect: 'disconnect',
  mongo_connected: 'mongo-connected',
  chatToBack: 'chatToBack', // toBack
  chatFromBack: 'chatFromBack', // fromBack
  chatNotifFromBack: 'chatNotifFromBack', // rodesac servers sheatkobines da is mimghebs gadascems
  chatNotifToBack: 'chatNotifToBack', // rodesac servers sheatkobines da is mimghebs gadascems da mimghebi upasuxebs
  updated: 'updated',
  typingToBack: 'typingToBack',
  typingFromBack: 'typingFromBack',
  fileToBack: 'fileToBack',
  fileStreamToBack: 'fileStreamToBack',
  fileSiofuToBack: 'fileSiofuToBack',
  fileFromBack: 'fileFromBack',
  networkError: 'networkError',
  tokenError: 'tokenError',
  downloadFileFromBack: 'downloadFileFromBack',
  downloadUserAllMessages: 'downloadUserAllMessages',
  downloadAllUsersForAdmin: 'downloadAllUsersForAdmin',
  downloadUserAllFileMessagesForAdmin: 'downloadUserAllFileMessagesForAdmin',
  loadUserOrdersForAdmin: 'loadUserOrdersForAdmin'
}
// aq rasac davcer, is iqneba key da mis mixedvit gadaitargmneba de.json-shi
export const errors = {
  no_invoice: 'no_invoice',
  failed: 'failed'
}
export const etexts = {
  IsNotExampleQuestionnaire: 'This is not an example questionnaire.',
  youHaveInvoiceDe: 'Eine Rechnung steht für Sie zur Verfügung.',
  youHaveInvoiceEn: 'An invoice is available for you.',
  youHaveQvaluesNotificationDeUnfilled: 'Ein nicht ausgefüllter Fragebogen steht für Sie zur Verfügung.',
  youHaveQvaluesNotificationDeFilled: 'Ein ausgefüllter Fragebogen steht für Sie zur Verfügung.',
  youHaveQvaluesNotificationEnUnfilled: 'A questionnaire that has not been completed is available for you.',
  youHaveQvaluesNotificationEnFilled: 'A completed questionnaire is available for you.',
  cardWasNotFound: 'card was not found.',
  noQuestionnaireFound: 'no questionnaire found!',
  getQuestionnaireFailed: 'get questionnaire failed!',
  getInvoiceFailed: 'get invoice failed!',
  noInvoiceFound: 'no invoice found!',
  uploadQuestionnaireFailed: 'questionnaire upload failed!',
  writeQuestionnaireToMongoFailed: 'Write questionnaire to mongodb failed!',
  sendQuestionnaireNotifToMainBackFailed: 'Send questionnaire notification to main back failed!',
  downloadInvoiceFailed: 'invoice download failed!',
  noInvoiceWasFound: 'invoice was not found!',
  noInvoiceDataInRequestObject: 'No invoice data was sent to server!',
  uploadInvoiceFailed: 'invoice upload failed!',
  invoiceNrFailed: 'get invoice nummer failed!',
  getDataByIdFailed: 'getDataById failed!',
  removeQmediaOnMongoFailed: 'Remove Qmedia file on Mongo failed!',
  translateTextFailed: 'translate text failed!',
  noDocumentsFound: 'found no documents!',
  requestError: 'request not valid!',
  updateDocumentFailed: 'failed to update document!'
}
export const status = {
  notSent: 'notSent',
  sent: 'sent',
  savedInDb: 'savedInDb',
  received: 'received',
  read: 'read'
}
// serverze chatis failebis atvirtvis metodebi
export const chatMediaSendMode = {
  axios: 'axios',
  siofu: 'siofu',
  socketStream: 'socketStream'
}
// service data from testers for orderers
// es itargmneba da amitom qveda tireebit aris
// eseni unda shevavsot sxva servisebit
export const invoiceParams = {
  G1: 'G1', // Fahrzeug checken
  G2: 'G2', // Fahrzeugcheckpaket kaufen
  G3: 'G3', // Anzahlung leisten
  check: 'check', // Fahrzeug checken
  resell: 'resell', // Fahrzeugcheckpaket kaufen
  pledge: 'pledge', // Anzahlung leisten
  tester: 'tester',
  orderer: 'orderer',
  buyer: 'buyer',
  basic: 'basic',
  advanced: 'advanced',
  urgency_standard: 'standard',
  urgency_urgent: 'urgent',
  // invoiceType-s
  check_AdminToChecker: 'check_AdminToChecker',
  check_CheckerToOrderer: 'check_CheckerToOrderer',
  resell_AdminToBuyer: 'resell_AdminToBuyer',
  resell_CheckerToAdmin: 'resell_CheckerToAdmin'
}
// data types
export const dataTypes = {
  text: 'text',
  file: 'file',
  video: 'video',
  audio: 'audio',
  image: 'image'
}
export const intents = {
  chat: 'chat',
  questionnaire: 'questionnaire',
  chatFilemessage: 'chatFilemessage',
  uploadQuestionnaire: 'uploadQuestionnaire',
  DOWNLOAD: 'download',
  DOWNLOADING: 'downloading',
  SIGNUP: 'signup',
  ANSWER: 'login',
  UPDATE_USER: 'updateUser',
  ADMIN_EXITED: 'adminExited'
}
// message types
export const mt = {
  TYPING: 'typing',
  TEXT: 'text',
  FILE: 'file',
  AVATAR: 'avatar',
  SYSTEM: 'system',
  SYSTEM_TIME: 'system_time'
}
// typing||onFocus||offFocus
export const typingState = {
  typing: 'typing',
  onFocus: 'onFocus',
  offFocus: 'offFocus'
}
// chatBus types
export const cbt = {
  SNACKBAR_INFO: 'snackbarInfo',
  DIALOG_INFO: 'dialogInfo',
  USE_SUGGESTION: 'useSuggestion',
  // LOGIN_SNACKBAR_INFO: 'loginSnackbarInfo',
  TO_ROOM: 'toRoom',
  TO_CHAT: 'toChat'
}

export const caller = {
  invoice: 'invoice',
  testerInvoice: 'testerInvoice',
  adminInvoice: 'adminInvoice',
  message: 'message',
  chat: 'chat',
  questionnaire: 'questionnaire',
  questionItem: 'questionItem',
  questionnaireCheckerValidation: 'questionnaireCheckerValidation',
  examplemedias: 'examplemedias',
  chatUit: 'chatUit',
  chatFilemessage: 'chatFilemessage'
}

export const collnames = {
  messages: 'messages',
  questionnaires: 'questionnaires'
}
export const checkerValidationStatus = {
  validateImages: 'validateImages',
  vin: 'vin',
  photo: 'photo',
  fill: 'fill'
}
export const staticPages = {
  contact: 'contact',
  gtc: 'gtc',
  imprint: 'imprint',
  teltesters: 'teltesters',
  testers: 'testers',
  orderers: 'orderers',
  principles: 'principles',
  privacy: 'privacy'
}
export const levels = {
  basic: 'basic',
  advanced: 'advanced',
  phone: 'phone'
}
export const questionnaireTypes = {
  phone: 'phone',
  deep: 'deep' // amashia advanced da basic
}
