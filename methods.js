// const interval = 24 * 60 * 60 * 1000
// aq ctx-ebis gareshe metodebia tavmokrili
export function compareVersion(v1, v2) {
  if (typeof v1 !== 'string') return false
  if (typeof v2 !== 'string') return false
  v1 = v1.split('.')
  v2 = v2.split('.')
  const k = Math.min(v1.length, v2.length)
  for (let i = 0; i < k; ++i) {
    v1[i] = parseInt(v1[i], 10)
    v2[i] = parseInt(v2[i], 10)
    if (v1[i] > v2[i]) return 1
    if (v1[i] < v2[i]) return -1
  }
  return v1.length === v2.length ? 0 : v1.length < v2.length ? -1 : 1
}
export const cloneObj = (obj) => {
  return JSON.parse(JSON.stringify(obj))
}
// valueObj-is mnishvnelobebi chaicereba updateObject-shi
export const updateObject = (updateObject, valueObj) => {
  if (!updateObject || valueObj) {
    return
  }
  for (const [key, value] of Object.entries(valueObj)) {
    updateObject[key] = value
  }
}
export const extractVin6FromVin = (vin) => {
  if (!vin) {
    return null
  }
  return vin.substring(vin.length - 6, vin.length)
}
export function removeLastCharacterFromString(str) {
  if (!str) {
    return ''
  }
  str = str.slice(0, -1)
  return str
}
export function toFixed2(price) {
  if (!price) {
    return 0
  }
  return Number.parseFloat(price).toFixed(2)
}
export function stp(prop) {
  return prop || ''
}
export function maxlength(str, len) {
  if (str) {
    if (str.length > len) {
      str = str.substring(0, len)
    }
    return str
  }
  return ''
}
export function valideVin(vin) {
  const vinReg = /\b[(A-H|J-N|P|R-Z|0-9)]{17}\b/
  return vinReg.test(vin)
}
export function valideVinLength(vin, len) {
  const vinReg = new RegExp('\\b[(A-Z|0-9)]{' + len + '}\\b')
  // const vinReg = /\b[(A-Z|0-9)]{6}\b/
  return vinReg.test(vin)
}
export function valideVinChar(vin) {
  const vinReg = /\b^[(A-H|J-N|P|R-Z|0-9)]+$\b/
  return vinReg.test(vin)
}
export function valideVin6(vin) {
  const vinReg = /\b[(A-H|J-N|P|R-Z|0-9)]{6}\b/
  return vinReg.test(vin)
}
// filename -> 156760011911441_14572.png
export const getTsAndSizeFromFilename = (filename) => {
  if (!filename || !filename.includes('_')) {
    return ''
  }
  if (filename.includes('.')) {
    filename = filename.substring(0, filename.indexOf('.'))
  }
  return filename.split('_')
}
export const getExtensionFromFilename = (filename) => {
  if (!filename) {
    return ''
  }
  const parts = filename.split('.')
  return parts.length > 1 ? parts.pop() : ''
}
export const getFilenamefromPath = (path) => {
  if (path) {
    return path.substring(path.lastIndexOf('/') + 1)
  }
  return ''
}
// itemValue.attachments-დან ამოშლის relPath-ს. იყენებს removeQMediaOnMongo
export const deleteRelPathInItemValueAttachments = (itemValue, relPath) => {
  if (!itemValue || !itemValue.attachments || !relPath) {
    return itemValue
  }
  Object.keys(itemValue.attachments).forEach((key) => {
    const value = itemValue.attachments[key]
    if (isArray(value) && value.includes(relPath)) {
      const result = value.filter((path) => path !== relPath)
      itemValue.attachments[key] = result
      return itemValue
    }
  })
  return itemValue
}
// relPath = '/questionnaires/1620597851020_1874893.jpeg'
export const getFilesizeFromRelPath = (path) => {
  if (path) {
    let filename = path.substring(path.lastIndexOf('/') + 1)
    if (filename.includes('.')) {
      filename = filename.substring(0, filename.indexOf('.'))
      if (filename && filename.includes('_')) {
        return filename.split('_')[1]
      }
    }
    return 0
  }
  return ''
}
export const isImage = (file) => {
  if (!file) {
    return false
  }
  const filename = file.name
  if (filename) {
    const ext = getExtensionFromFilename(filename)
    switch (ext.toLowerCase()) {
      case 'jpg':
      case 'jpeg':
      case 'gif':
      case 'bmp':
      case 'png':
      case 'webp':
        // etc
        return true
    }
  } else if (file.type && file.type.includes('image')) {
    return true
  }
  return false
}
export const isVideo = (file) => {
  if (!file) {
    return false
  }
  const filename = file.name
  if (filename) {
    const ext = getExtensionFromFilename(filename)
    switch (ext.toLowerCase()) {
      case 'm4v':
      case 'x-m4v':
      case 'avi':
      case 'mpg':
      case 'mp4':
      case 'webm':
      case 'mov':
      case 'quicktime': // es apple-is aris .mov -s ase ezaxian
      case 'ogv':
        // etc
        return true
    }
  } else if (file.type && file.type.includes('video')) {
    return true
  }
  return false
}
export const isAudio = (file) => {
  if (!file) {
    return false
  }
  const filename = file.name
  if (filename) {
    const ext = getExtensionFromFilename(filename)
    switch (ext.toLowerCase()) {
      case 'm4a':
      case 'mp3':
      case 'wav':
      case 'wma':
      case 'aac':
      case 'flac':
      case 'x-m4a':
      case 'ogg':
      case 'adts':
        // etc
        return true
    }
  } else if (file.type && file.type.includes('audio')) {
    return true
  }
  return false
}

export const expandArrayText = (arr) => {
  let s = ''
  for (const el of arr) {
    s += el
  }
  return s
}
export const readStaticTexts = (payload) => {
  payload.relDir = 'staticTexts/'
  return readTextfile(payload)
}
// kitxulobs did teqstian teqst failebs, mag. locales/de/infoText.txt
// payload = { relDir, locale, filename, version }  relDir = ../locales/staticTexts/
export const readTextfile = (payload) => {
  // this.$g.clog('m locale ', locale)
  const relDir = payload.relDir
  const locale = payload.locale
  const filename = payload.filename
  const version = payload.version
  // const ext = payload.ext ? payload.ext : 'txt'
  try {
    // tu fails versiebi aqvs mashin es daemateba
    const _version = version ? '/' + version : '' // /locales/staticTexts/de/gtc/1.0.0.txt
    const t = require('raw-loader!../locales/' + relDir + locale + '/' + filename + _version + '.txt')
    if (t) {
      const infoText = t.default
      // this.$g.clog('m this.infoText.length = ', infoText.length)
      return infoText
    }
  } catch (error) {
    return ''
  }
  return ''
}
export const readDeepQuestionnaireJSonfile = (version) => {
  return readJSonfile('questionnaires/deep/', version)
}
export const readPhoneQuestionnaireJSonfile = (version) => {
  return readJSonfile('questionnaires/phone/', version)
}
/* export const readQuestionnaireJSonfile = (version, type) => {
  return readJSonfile('questionnaires/' + type + '/', version)
} */
export const readLangFromKeysJSonfile = (locale) => {
  return readJSonfile('utils/', 'langKeys').langKeys[locale]
}
export const readVideoTutorialsFromKeysJSonfile = (locale) => {
  return readJSonfile('utils/', 'videoTutorials').videoTutorials[locale]
}
export const readLangJSonfile = (locale) => {
  return readJSonfile('', locale)
}
export const readCountryFromCountryCodesJSonfile = (code) => {
  return readJSonfile('utils/', 'countryList').codes[code]
}
export const readGVersionsJsonfile = () => {
  return readJSonfile('utils/', 'main_versions')
}
export const readOtherTextsJSonfile = (filename) => {
  return readJSonfile('otherTexts/', filename)
}
//  relDir = ../locales/utils/   filename = countryList
export const readJSonfile = (relDir, filename) => {
  try {
    // aq require-shi shignit aucileblad unda ickebodes ../ -it da ara variableshi ikos chacerili
    const t = require('../locales/' + relDir + filename + '.json')
    // console.log('mms t', t)
    // return require(relDir + filename + '.json')
    if (t) {
      // const infoText = t.default
      return t
    }
  } catch (error) {
    console.error('error ', error)
    return ''
  }
}
export function objectLooper(obj) {
  for (const [key, value] of Object.entries(obj)) {
    console.log('key ', key)
    console.log('value ', value)
  }
  return obj
}
export function isArray(obj) {
  return !!obj && obj.constructor === Array
}
export function isObject(obj) {
  return !!obj && obj.constructor === Object
}
export function isString(obj) {
  return !!obj && obj.constructor === String
}
